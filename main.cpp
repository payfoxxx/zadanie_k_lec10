#include "pch.h"
#include <iostream>

using namespace std;

double my_pow(double a,int k) {
	double res;
	if (k > 0) {
		res = a;
		for (int i = 1; i < k; i++) {
			res *= a;
		}
	}
	if (k < 0) {
		res = 1 / a;
		for (int i = -1; i > k; i--) {
			res *= (1 / a);
		}
	}
	return res;
}

int main()
{
	setlocale(LC_ALL, "rus");
	double a;
	int k;
	cout << "Введите число: ";
	cin >> a;
	cout << "\nВведите степень: ";
	cin >> k;
	double aink;
	aink = my_pow(a, k);
	cout << "\nРезультат: " << aink;
}


